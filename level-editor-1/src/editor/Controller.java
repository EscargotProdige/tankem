package editor;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.input.DragEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Box;
import javafx.scene.transform.Rotate;
import javafx.stage.Stage;
import map.Map;

import java.util.ArrayList;

/**
 * Created by 1586848 on 2018-04-04.
 */
public class Controller {

    @FXML private SubScene render_subscene;
    private Group sceneRoot;
    private ArrayList<ArrayList<BoxWrapper>> boxes;
    private Map currentMap;
    @FXML private VBox sideVBox;
    @FXML private AnchorPane mapInfoPane;
    @FXML private MenuBar menuBar;
    @FXML private VBox wholeMenuVBox;

    //toggles pour le mode d'édition
    @FXML private RadioButton fixedUp;
    @FXML private RadioButton fixedDown;
    @FXML private RadioButton moveUp;
    @FXML private RadioButton moveDown;
    @FXML private ToggleGroup wall;

    //labels pour l'information sur la tuile courante
    @FXML private Label etatTuile;
    @FXML private Label spawnIndicateur;

    //sliders pour la largeur de la map
    @FXML private Slider largeurMap;
    @FXML private Slider hauteurMap;

    private Group cameraGroup = new Group();
    private Stage stage;

    @FXML public void initialize(){
        currentMap = buildRandomMap(12,12);

        boxes = buildScene(currentMap);

        sceneRoot = ((Group)render_subscene.getRoot());

        EventHandler<MouseEvent> editHandler = buildEditHandler();

        render_subscene.setOnMouseClicked(editHandler);
        render_subscene.setOnMouseDragged(editHandler);

        hauteurMap.setMajorTickUnit(1);
        hauteurMap.setSnapToTicks(true);
        largeurMap.setMajorTickUnit(1);
        largeurMap.setSnapToTicks(true);

        hauteurMap.setValue(currentMap.getHeight());
        largeurMap.setValue(currentMap.getWidth());
        hauteurMap.valueChangingProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                //forcer que la fonction s'exécute dans le "JavaFX Application Thread".
                Platform.runLater(()->{
                    editRowCount(currentMap,(int)hauteurMap.getValue());
                });
            }
        });
        largeurMap.valueChangingProperty().addListener(new ChangeListener<Boolean>() {

            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                //forcer que la fonction s'exécute dans le "JavaFX Application Thread".
                Platform.runLater(()->{
                    editColumnCount(currentMap,(int)largeurMap.getValue());
                });
            }
        });
    }
    /**
     * cette fonction construit la scène en fonction de la carte qui lui est passée en paramètre.
     * @param map la carte à représenter
     * @return une ArrayList comprenant les différents objets javafx.scene.shape.Box présents.
     */
    private ArrayList<ArrayList<BoxWrapper>> buildScene(Map map){
        ArrayList<ArrayList<BoxWrapper>> returnVal = new ArrayList<ArrayList<BoxWrapper>>();
        for (int i = 0; i < map.getHeight(); i++) {
            returnVal.add(new ArrayList<BoxWrapper>());
            for (int j = 0; j < map.getWidth(); j++) {
                returnVal.get(i).add(null);
            }
        }
        sceneRoot = new Group();
        render_subscene.setRoot(sceneRoot);
        PointLight pl = new PointLight(Color.LIGHTGREEN);
        pl.setTranslateY(-200);
        pl.setTranslateZ(-200);
        sceneRoot.getChildren().add(pl);
        PerspectiveCamera camera = buildCamera();
        sceneRoot.getChildren().add(camera);
        render_subscene.setCamera(camera);
        ArrayList<ArrayList<Integer>> mapGrid = map.getMapGrid();
        int i = 0;
        for(ArrayList<Integer> b:mapGrid){
            int j = 0;
            for(Integer val:b){
                BoxWrapper wrapper = new BoxWrapper(i,j,map);
                Box box = wrapper.getBox();
                sceneRoot.getChildren().add(box);
                returnVal.get(i).set(j, wrapper);
                j++;
            }
            i++;
        }
        return returnVal;
    }

    private void editRowCount(Map map, int newRowCount){
        if(newRowCount > map.getHeight()){
            int delta = newRowCount - map.getHeight();
            for (int i = 0; i < delta; i++) {
                ArrayList<Integer> newRow = new ArrayList<Integer>();
                for (int j = 0; j < map.getWidth(); j++) {
                    newRow.add(0);
                }
                map.getMapGrid().add(newRow);
            }
        }
        else if(newRowCount < map.getHeight()){
            int delta = map.getHeight() - newRowCount;
            for (int i = 0; i < delta; i++) {
                map.getMapGrid().remove(map.getMapGrid().size()-1);
            }
        }
        map.setHeight(newRowCount);
        boxes = buildScene(map);
    }
    private void editColumnCount(Map map, int newColumnCount){
        if(newColumnCount > map.getWidth()){
            int delta = newColumnCount-map.getWidth();
            for (int i = 0; i < delta; i++) {
                for(ArrayList<Integer> row:map.getMapGrid()){
                    row.add(0);//add the cell to the row
                }
            }
        }
        if(newColumnCount < map.getWidth()){
            int delta = map.getWidth()-newColumnCount;
            for (int i = 0; i < delta; i++) {
                for(ArrayList<Integer> row:map.getMapGrid()){
                    row.remove(row.size()-1);//add the cell to the row
                }
            }
        }
        map.setWidth(newColumnCount);
        boxes = buildScene(map);
    }


    private EventHandler<MouseEvent> buildEditHandler(){
        return new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                int mode = (wall.getSelectedToggle() == fixedDown) ? 0 : (wall.getSelectedToggle() == moveUp) ? 4 : (wall.getSelectedToggle() == moveDown) ? 3 : 1;
                //System.out.println("mode:"+mode);
                Node result = event.getPickResult().getIntersectedNode();
                if(result != null){
                    for (int row = 0; row < currentMap.getHeight(); row++) {
                        for (int col = 0; col < currentMap.getWidth(); col++) {
                            if(boxes.get(row).get(col).getBox() == result){
                                sceneRoot.getChildren().remove(result);
                                currentMap.getMapGrid().get(row).set(col, mode);
                                BoxWrapper wrapper = new BoxWrapper(row, col, currentMap);
                                Box addedBox = wrapper.getBox();
                                addedBox.setOnMouseEntered(new EventHandler<MouseEvent>() {
                                    @Override
                                    public void handle(MouseEvent event) {
                                        int state = wrapper.getState();
                                        etatTuile.setText("état de la tuile: "+((state == 0) ? "fixe au sol" : (state == 1) ? "fixe en haut" : (state == 2) ? "mobile (direction 1)" : "mobile (direction 2)"));
                                        spawnIndicateur.setText("spawn: "+wrapper.getSpawnHere());
                                    }
                                });
                                boxes.get(row).set(col, wrapper);
                                sceneRoot.getChildren().add(addedBox);
                            }
                        }
                    }
                }
                else{
                    spawnIndicateur.setText("Aucune tuile Sélectionnée");
                    etatTuile.setText("");
                }
            }
        };
    }


    private Map buildRandomMap(int rows, int cols){
        return new Map(rows, cols);
    }

    private PerspectiveCamera buildCamera() {
        double dist = 1000;
        PerspectiveCamera returnVal = new PerspectiveCamera(true);
        returnVal.setNearClip(1);
        returnVal.setFarClip(10000);
        returnVal.setTranslateZ(-dist);
        returnVal.setTranslateY(-dist);
        returnVal.getTransforms().addAll(new Rotate(-45, Rotate.X_AXIS));
        return returnVal;
    }

    public void setStageAndBindResizeEvents(Stage stage){
        this.stage = stage;
        stage.heightProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                setSubceneHeight(newValue.doubleValue()-(menuBar.getHeight()+40));
            }
        });
        stage.widthProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                setSubceneWidth(newValue.doubleValue()-sideVBox.getWidth());
            }
        });
    }
    @FXML private void setSubceneHeight(double height){
        render_subscene.setHeight(height);
    }
    @FXML private void setSubceneWidth(double width){
        render_subscene.setWidth(width);
    }


}
