package editor;

import javafx.event.EventHandler;
import javafx.scene.input.DragEvent;
import javafx.scene.shape.Box;
import map.Map;

import java.util.ArrayList;

public class BoxWrapper {
    private static double cubeSize = 50;

    private Box box;
    private int col;
    private int row;
    private int state;
    private int spawnHere;

    public BoxWrapper(int row, int col, Map map){
        this.row = row;
        this.col = col;
        this.box = buildBoxForMap(map, row, col);
        this.state = map.getMapGrid().get(row).get(col);
        this.spawnHere = 0;
    }

    private Box buildBoxForMap(Map map, int row, int col){
        ArrayList<ArrayList<Integer>> mapGrid = map.getMapGrid();
        int val = mapGrid.get(row).get(col);
        Box box = new Box(cubeSize,(val > 0) ? cubeSize : 1,cubeSize);
        box.setTranslateY(val == 1 ? -box.getHeight()/2 : box.getHeight()/2);
        box.setTranslateX((map.getWidth()%2 == 0 ? col*cubeSize : col*cubeSize - cubeSize/2)- (map.getWidth()/2)*cubeSize);
        box.setTranslateZ((map.getHeight()%2 == 0 ? row*cubeSize : row*cubeSize - cubeSize/2) - (map.getHeight()/2)*cubeSize);
        return box;
    }

    public Box getBox() {
        return box;
    }

    public int getState(){
        return this.state;
    }
    public int getSpawnHere(){
        return spawnHere;
    }
}
