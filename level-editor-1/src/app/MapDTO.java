package app;

import map.Map;

import java.util.ArrayList;

/**
 * Created by 1586848 on 2018-04-11.
 */
public class MapDTO {

    private Map currentMap;
    private ArrayList<Map> mapList;

    public MapDTO(){
        mapList = new ArrayList<Map>();
    }

    public MapDTO(boolean randomize){
        mapList = new ArrayList<Map>();
        if(randomize){
            for (int i = 0; i < (int) Math.random() * 25; i++) {
                addMap(new Map((int)(Math.random()*6+6),(int)(Math.random()*6+6)));
            }
        }
    }
    public void setCurrentMap(Map map){
        currentMap = map;
    }

    public void addMap(Map map){
        mapList.add(map);
    }

    public ArrayList<Map> getMapList() {
        return mapList;
    }
}
