package app;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.stage.Stage;
import map.Map;

import java.io.IOException;

public class Controller {
    @FXML
    private TableView<Map> mapsTableView;

    @FXML
    public void initialize() {
        MapDTO dto = new MapDTO(true);
    }

    @FXML
    private Button newButton;

    @FXML
    private void newMap() {
        Stage stage = (Stage) newButton.getScene().getWindow();
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("editor-layout.fxml"));
            Parent root = loader.load();
            editor.Controller controller = loader.getController();
            controller.setStageAndBindResizeEvents(stage);
            stage.setResizable(true);
            stage.setScene(new Scene(root));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
