package app;

import map.Map;
import java.sql.*;  

import java.util.ArrayList;

public class MapDAO {

    // Variables
    Connection con = null;
    MapDTO mapdto = null;

    //Constructeur
    public MapDAO(MapDTO mapdto){
        this.mapdto = mapdto;
    }

    //getters
    public Connection getCon(){
        return con;
    }

    public MapDTO getDto(){
        return mapdto;
    }

    //Méthodes Insertions
    public void insertMap(Map map){
        String nom = map.getNom();
        int status = map.getStatus();
        double delaiMin = map.getMinItemSpawn();
        double delaiMax = map.getMaxItemSpawn();
        int spawns[][] = map.getSpawns();
        int height = map.getHeight();
        int width = map.getWidth();

        Statement stmt = null;
//        String query = "INSERT INTO niveau (nom, statCode, delaiMin, delaiMax, spawnAx, spawnAy," +
//            "spawnBx, spawnBy, nbX, nbY)" +
//            "VALUES ('?', ?, ?, ?, ?, ?)", nom, status, delaiMin, delaiMax, spawns[0][0], spawns[0][1], spawns[1][0], spawns[1][1], height, width);
    }   

    // A TERMINER!!!!



    //Méthodes de connexion
    public void connexion(){

        try{
            // Établir connexion
            Class.forName("oracle.jdbc.driver.OracleDriver");

            con=DriverManager.getConnection(  
            "jdbc:oracle:thin:@localhost:1521:xe","e1586848","A");

            System.out.println("Connection réussi!");
        }
        catch(SQLException e){
            System.out.println("Connection Failed!");
            e.printStackTrace();
        }
        catch(Exception e){
            System.out.println("Exception problem.");
            e.printStackTrace();
        }
    }

    public void deconnexion(){
        try{
            if (con != null){
                con.close();
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }
}