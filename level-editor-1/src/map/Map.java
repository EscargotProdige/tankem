package map;

import java.util.ArrayList;

/**
 * Created by 1586848 on 2018-04-04.
 */
public class Map {
	//variables
	private String nom;
	private int status;
    private int width;
    private int height;
    private int[][] spawns;
    private double min_item_spawn;
    private double max_item_spawn;

    private ArrayList<ArrayList<Integer>> mapGrid;
    private ArrayList<ArrayList<Boolean>> treeGrid;
	
	//constructeur
    public Map(int height, int width){
        this.height = height;
        this.width = width;
        mapGrid = new ArrayList<ArrayList<Integer>>();
        treeGrid = new ArrayList<ArrayList<Boolean>>();
        for (int i = 0; i < height; i++) {
            mapGrid.add(new ArrayList<Integer>());
            treeGrid.add(new ArrayList<Boolean>());
            for (int j = 0; j < width; j++) {
                double random = Math.random();
                mapGrid.get(i).add((random < 0.7) ? 0: (random < 0.8) ? 1:2);
                treeGrid.get(i).add(random < 0.05);
            }
        }
        spawns = new int[][]{{(int) Math.random() * height, (int) Math.random() * width}, {(int) Math.random() * height, (int) Math.random() * width}};

        min_item_spawn = Math.random() * 10;
        max_item_spawn = Math.random() * 10 + min_item_spawn;
    }
	
	
	//getters
	public String getNom(){
		return nom;
	}
	
	public int getStatus(){
		return status;
	}
	
    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

	
	public int[][] getSpawns(){
		return spawns;
	}	
	
	public double getMinItemSpawn(){
		return min_item_spawn;
	}
	
	public double getMaxItemSpawn(){
		return max_item_spawn;
	}
	
	public ArrayList<ArrayList<Integer>> getMapGrid() {
        return mapGrid;
    }
	
	public ArrayList<ArrayList<Boolean>> getTreeGrid() {
        return treeGrid;
    }
	
	//méthodes


    public void setWidth(int width) {
        this.width = width;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String validate(){

        String message = "MAP_VALID";

        if(this.width > 12 || this.width < 6 || this.height > 12 || this.height < 6){
            appendErrorMessage(message, "SIZE_INVALID");
        }
        if(this.spawns.length < 2){
            appendErrorMessage(message, "SPAWNS_MISSING");
        }
        for (int i = 0; i < this.spawns.length; i++) {
            for (int j = 0; j < 2; j++) {
                if(j == 0){
                    if(this.spawns[i][j] < 0 || this.spawns[i][j] > this.width){
                        appendErrorMessage(message, "SPAWNS_INVALID");
                        break;
                    }
                }
                else if(j == 1){
                    if(this.spawns[i][j] < 0 || this.spawns[i][j] > this.height){
                        appendErrorMessage(message, "SPAWNS_INVALID");
                        break;
                    }
                }
            }
        }
        if(min_item_spawn > max_item_spawn){
            appendErrorMessage(message, "ITEM_SPAWNS_INVALID");
        }

        return message;
    }

    private String appendErrorMessage(String initialMessage, String append){
        return initialMessage.equals("MAP_VALID") ? append : initialMessage.concat(" "+append);
    }
}
