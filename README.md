# TANKEM PHASE 2 #######################################################################################

	Equipe :
		Vincent, Laurier, Zénon, Kevin

	Tâches :
		DAO Python : Vincent, Kevin(Tests et corrections)
		DTO Python : Vincent, Kevin(Tests et corrections)
		DAO Java : Laurier(Principalement responsable du produit final), Vincent(Initialisation)
		Selecteur de map : Kevin
		Editeur de niveau : Zénon, Laurier

# Instructions ##########################################################################################

	Selecteur de niveau : 
		- Arrow up et down : Bouge le curseur
		- Enter : Demarre le jeu avec la map selectionée (curseur)
		
	Editeur de niveau : 
		* Note: Le dossier git de l'editeur se trouve dans le fichier ZIP "level-editor-1\level-editor-archive-with-git-data.zip" *
		
		- Ouvrir "Editeur de Niveau.jar"
		- Deux sliders permettent de selectionner les dimension X et Y de la map
		- Deux comboBox permettent de selectionner les delais MIN et MAX du niveau
		
		- Modificateur de tuile : Cliquer sur la map modifie la tuile cliquée avec les paramètres choisis.
		
			- Selection de spawn :
				- Les boutons Spawn1 et Spawn2 permettent de choisir les spawn points pour les deux tanks
				- Ils overwrite tout autre modificateurs, il s'appliquent seul en premier et peuvent être modifiés par la suite			
		
			- Selection de tuiles : 
				- Arbre : Ajoute un arbre au modifieur de tuile
				- Fixe en haut : Ajoute un mur fixe surélevé au modifieur de tuile
				- Fixe en bas  : Perment d'effacer une tuile 
				- Mobile       : Ajoute un mur animé au modifieur de tuile
				- Mobile inverse : Ajoute un mur animé surélevé (inverse) au modifieur de tuile
				
			- Bouton simulate et texte en bas :
				- Scrapped, non-fonctionnels
				