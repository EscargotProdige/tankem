## -*- coding: utf-8 -*-
#Ajout des chemins vers les librarires
from util import inclureCheminCegep
import sys

#Importe la configuration de notre jeu
from panda3d.core import loadPrcFile
loadPrcFile("config/ConfigTankem.prc")

#Module de Panda3DappendObject
from direct.showbase.ShowBase import ShowBase

#Modules internes
from gameLogic import GameLogic
from interface import InterfaceMenuPrincipal, InterfaceMapSelector


#Modules necessaires au fonctionnement de la balance et des maps
from data import *

 
class Tankem(ShowBase):
    def __init__(self, balanceDTO , mapDTO):
        ShowBase.__init__(self)
        self.demarrer(balanceDTO, mapDTO)

    def demarrer(self, balanceDTO , mapDTO):        
        self.gameLogic = GameLogic(self, balanceDTO, mapDTO)
        #Commenter/décommenter la ligne de votre choix pour démarrer le jeu
        #Démarre dans le menu
        self.menuPrincipal = InterfaceMenuPrincipal()  

        # PHASE 2 ##################################################################################################
        self.accept("loadSelector",lambda: InterfaceMapSelector(mapDTO))
        ############################################################################################################  

        #Démarre directement dans le jeu
        #messenger.send("DemarrerPartie")

    

#Main de l'application.. assez simple!
dto = Balance_DTO()
balance_dto = Balance_DTO()
map_dto = Map_DTO()

try:     
    dao = Balance_Oracle_DAO()
    dao.getGameAttributes(dto)   
except:    
    dto.textData["message_lobby_content"] = "Problème de connexion. Configuration par défaut utilisée."
    dto.numData["message_lobby_duration"] = 6

app = Tankem(dto,map_dto)
app.run()