# -*- coding: utf-8 -*-
from __future__ import print_function
from BaseDAO import BaseDAO
from BalanceDTO import Balance_DTO
from constants import *
import cx_Oracle
import sys

class Balance_Oracle_DAO(BaseDAO):
	"""DAO servant aux accès avec une base de donnée Oracle"""
	def __init__(self):	
		super(Balance_Oracle_DAO,self).__init__()		
		
		# Init de la connection		
		sys.path.append(PATH_ORACLE)    
		dsn_tns = cx_Oracle.makedsn(DB_HOST, 1521, DB_SID)
		chaineConnexion = DB_USERLOGIN + '/' + DB_PASSWORD + '@' + dsn_tns   
		
		# Assignation des variables
		self.connexion = cx_Oracle.connect(chaineConnexion)
		self.cur = self.connexion.cursor()	

	########################################################################
	# Methodes internes
	########################################################################
	def getAttributesFromTable(self,targetTable,balanceDTO):
		if targetTable:
			#Query
			query = "SELECT * FROM " + targetTable
			self.cur.execute(query)
			
			#Target
			if targetTable == NUMERIC_TABLE:
				targetDict = balanceDTO.numData
			else:
				targetDict = balanceDTO.textData

			#Cursor	
			for row in self.cur.fetchall(): 						
				self.isValid(targetDict,row[ATTRIBUTE_NAME])		
				#Encode text values
				if targetTable == NUMERIC_TABLE:
					targetDict[row[ATTRIBUTE_NAME]] = row[ATTRIBUTE_VALUE]
				else:
					targetDict[row[ATTRIBUTE_NAME]] = row[ATTRIBUTE_VALUE].decode("cp1252").encode("utf8")

				#Insert min and max (for CSV and faster Database processing (ID))
				targetDict[row[ATTRIBUTE_NAME]+GET_ID] = row[ATTRIBUTE_ID]	
				targetDict[row[ATTRIBUTE_NAME]+GET_MIN] = row[ATTRIBUTE_MIN]						
				targetDict[row[ATTRIBUTE_NAME]+GET_MAX] = row[ATTRIBUTE_MAX]				

	def updateAttributesFromTable(self,targetTable,balanceDTO):
		if targetTable:			
			#Params
			paramsVal = []
			paramsMin = []
			paramsMax = []    

			#targetTable
			if targetTable == NUMERIC_TABLE:
				targetDict = balanceDTO.numData
			else:
				targetDict = balanceDTO.textData
			
			# Note : It parses the dictionary keys to find out which column to update
			for (name,value) in targetDict.iteritems():				
				#Note: We also need to verify if the ID is in the dictionary, which only happens if that parameter wasn`t inserted
				#that way we only insert the values from the DB and not the ones that in invalid in the game`s code
				if(GET_ID not in name and name+GET_ID in targetDict):	
					targetID = targetDict[name+GET_ID]				
					#Update the MAX							
					valMax = targetDict[name+GET_MAX]							
					paramsMax.append((valMax,targetID))  			
				
					#Update the MIN			
					valMin = targetDict[name+GET_MIN]							
					paramsMin.append((valMin,targetID))  				
				
					#Update the VAL				
					paramsVal.append((value,targetID)) 	

			#Insertions			
			if len(paramsMax)>0: 			
				query = "UPDATE " + targetTable + " SET "+ COL_MAX +"= :1 WHERE id = :2"
				self.cur.executemany(query, paramsMax, batcherrors=True)
				print(query)
				print(paramsMax)
				self.connexion.commit()  

			if len(paramsMin)>0: 							
				query = "UPDATE " + targetTable + " SET "+ COL_MIN +"= :1 WHERE id = :2"
				self.cur.executemany(query, paramsMin, batcherrors=True)
				self.connexion.commit()		

			if len(paramsVal)>0: 			
				query = "UPDATE " + targetTable + " SET "+ COL_VALUE +"= :1 WHERE id = :2"				
				self.cur.executemany(query, paramsVal, batcherrors=True)
				self.connexion.commit()   	
	
	########################################################################
	# Methodes héritées
	########################################################################	
	def getGameAttributes(self,balanceDTO):
		self.getAttributesFromTable(NUMERIC_TABLE,balanceDTO)
		self.getAttributesFromTable(TEXT_TABLE,balanceDTO)	
		self.showErrors(balanceDTO)	

	def updateGameAttributes(self,balanceDTO): 		
		self.updateAttributesFromTable(NUMERIC_TABLE,balanceDTO)
		self.updateAttributesFromTable(TEXT_TABLE,balanceDTO)	
	
