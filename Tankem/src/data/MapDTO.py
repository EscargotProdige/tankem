# -*- coding: utf-8 -*-
class Map_DTO(object):
	"""DTO servant aux transferts de donnees"""
	def __init__(self):			
		#Tableau contenant le nom des maps
		self.nomActifData = []

		#Dictionnaire contenant les informations du niveau choisi
		self.infoNiveau = {}
		
		#Tableau contenant les Cellules du niveau choisi
		self.infoCells = []

		
		