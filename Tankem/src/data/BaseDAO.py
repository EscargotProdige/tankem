## -*- coding: utf-8 -*-
from sanitizer import Sanitizer
class BaseDAO(object):
	"""DAO abstrait servant de patron pour les sous-classes"""
	def __init__(self):	
		self.sanitizer = Sanitizer()  

	def getGameAttributes(self,balanceDTO):
		pass

	def updateGameAttributes(self,balanceDTO):
		pass

	def isValid(self,targetDict,insertedData):
		return self.sanitizer.isValid(targetDict,insertedData)

	def isNumber(self,paramName,insertedNumber):
		return self.sanitizer.isNumber(paramName,insertedNumber)

	def showErrors(self,balanceDTO):
		self.sanitizer.showErrors(balanceDTO)