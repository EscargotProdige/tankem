# -*- coding: utf-8 -*-
class Balance_DTO(object):
	"""DTO servant aux transferts de donnees"""
	def __init__(self):		
		self.numData = {}
		self.textData = {}

		#Note: The Min/Max values will be added from the Database for internal verifications 
		#Note: The default value the game will use in case of a database connection/access failure
		
		# Numerical numData =========================================================
		self.numData["tank_speed"] = 7.0
		self.numData["tank_rotation"] = 1500.0
		self.numData["tank_health"] = 200.0

		self.numData["map_block_speed"] = 0.8		

		self.numData["cannon_speed"] = 14.0 
		self.numData["cannon_cooldown"] = 1.2
		
		self.numData["smg_speed"] = 18.0
		self.numData["smg_cooldown"] = 0.4

		self.numData["grenade_speed"] = 16.0
		self.numData["grenade_cooldown"] = 0.8

		self.numData["shotgun_speed"] = 13.0
		self.numData["shotgun_cooldown"] = 1.8
		self.numData["shotgun_barrel_size"] = 0.4

		self.numData["trap_speed"] = 1.0
		self.numData["trap_cooldown"] = 0.8

		self.numData["missile_speed"] = 30.0
		self.numData["missile_cooldown"] = 3.0

		self.numData["spring_speed"] = 10.0
		self.numData["spring_cooldown"] = 0.5

		self.numData["shot_explosion_radius"] = 8.0

		self.numData["message_lobby_duration"] = 3.0 
		self.numData["message_countdown_duration"] = 3.0		

		# Text numData ==============================================================
		self.textData["message_lobby_content"] = "Appuyez sur F1 pour l'aide"
		self.textData["message_gamestart_content"] = "Tank'em!"
		self.textData["message_gameover_content"] = "Joueur %s a gagné!"
		
		



