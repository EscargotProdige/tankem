# -*- coding: utf-8 -*-
from __future__ import print_function
from BaseDAO import BaseDAO
from MapDTO import Map_DTO
from constants import *
import cx_Oracle
import sys

class DAO_Map_Oracle(BaseDAO):
	"""DAO servant aux acc�s avec une base de donn�e Oracle"""
	def __init__(self):	
		super(DAO_Map_Oracle,self).__init__()	

		# Init de la connection		
		sys.path.append(PATH_ORACLE)    
		dsn_tns = cx_Oracle.makedsn(DB_HOST, 1521, DB_SID)
		chaineConnexion = DB_USERLOGIN + '/' + DB_PASSWORD + '@' + dsn_tns   
		
		# Assignation des variables
		self.connexion = cx_Oracle.connect(chaineConnexion)
		self.cur = self.connexion.cursor()
	
	#Retourne nom de tous les niveaux ayant un status Actif
	def getNiveauxActif(self, MapDTO):	
		#Query
		query = "SELECT nom FROM Niveau WHERE statCode = 1 ORDER BY nom ASC"
		self.cur.execute(query)

		#Enregistre DTO
		for nom in self.cur.fetchall():			
			MapDTO.nomActifData.append(nom[0])		


	#Retourne les informations complète d'un niveau spécifique selon son nom
	def getNiveauParNom(self,nom, MapDTO):	
		#Query
		query = "SELECT * FROM Niveau WHERE nom = '" + nom+"'"		
		self.cur.execute(query)

		#Enregistre DTO
		temp = {}
		temp = self.cur.fetchall()

		if len(temp) > 0:
			MapDTO.infoNiveau['id'] = temp[0][0]
			MapDTO.infoNiveau['nom'] = temp[0][1]
			MapDTO.infoNiveau['statCode'] = temp[0][2]
			MapDTO.infoNiveau['delaiMin'] = temp[0][3]
			MapDTO.infoNiveau['delaiMax'] = temp[0][4]
			MapDTO.infoNiveau['spawnAx'] = temp[0][5]
			MapDTO.infoNiveau['spawnAy'] = temp[0][6]
			MapDTO.infoNiveau['spawnBx'] = temp[0][7]
			MapDTO.infoNiveau['spawnBy'] = temp[0][8]
			MapDTO.infoNiveau['nbX'] = temp[0][9]
			MapDTO.infoNiveau['nbY'] = temp[0][10]
			
			#Récupéré info cellules	
			query = "SELECT * FROM Cellule WHERE mapId = " + str(MapDTO.infoNiveau['id'])
			self.cur.execute(query)
			
			#Enregistre dans DTO
			temp = self.cur.fetchall()
			for cell in temp:
				tmpCell = [(cell[2],cell[3],cell[4],cell[5])]
				MapDTO.infoCells.append(tmpCell)
	