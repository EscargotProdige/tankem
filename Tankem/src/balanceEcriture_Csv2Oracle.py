# -*- coding: utf-8 -*-
import os, cx_Oracle
from data import *
from Tkinter import Tk
from tkFileDialog import askopenfilename
import tkMessageBox 

def select_open_csv():
    Tk().withdraw()
    desktop = os.path.join(os.path.join(os.environ['USERPROFILE']), 'Desktop') 
    return askopenfilename(
        filetypes=[("CSV files", "*.csv"), ("All files", "*")], defaultextension="*.csv",  initialdir=desktop)

def main():
    fichier = select_open_csv()

    try:
        dto = Balance_DTO()
        oracle_dao = Balance_Oracle_DAO()
        csv_dao = Balance_CSV_DAO()

        if(csv_dao.getGameAttributes(dto, fichier)):           
            oracle_dao.updateGameAttributes(dto)   
            tkMessageBox.showinfo("TRANSFERT", "Données du CSV insérées dans la base de données.")	      
     

    except IOError:
        tkMessageBox.showinfo("ERREUR", "Impossible de lire le fichier " + fichier)	
    except cx_Oracle.DatabaseError:
        tkMessageBox.showinfo("ERREUR", "Impossible de se connecter à la base de données")	      

if __name__ == '__main__':
    main()
